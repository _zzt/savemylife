import sys, pygame, os
from pygame.compat import geterror
from pygame.locals import *

if not pygame.font: print ('Warning, fonts disabled')
if not pygame.mixer: print ('Warning, sound disabled')

main_dir = os.path.split(os.path.abspath(__file__))[0]
data_dir = os.path.join(main_dir, 'resources')

SCREENWIDTH, SCREENHEIGHT = 800, 600
clock = pygame.time.Clock()
currentmap = None

dirty = False

#functions to create our resources
def load_image(name, colorkey=None):
	fullname = os.path.join(data_dir, name)
	try:
		image = pygame.image.load(fullname)
	except pygame.error:
		print ('Cannot load image:', fullname)
		raise SystemExit(str(geterror()))
	image = image.convert()
	if colorkey is not None:
		if colorkey is -1:
			colorkey = image.get_at((0,0))
		if colorkey is -2:
			colorkey = image.get_at((6, 25))
		image.set_colorkey(colorkey, RLEACCEL)
	return image, image.get_rect()

def load_sound(name):
	class NoneSound:
		def play(self): pass
	if not pygame.mixer or not pygame.mixer.get_init():
		print "NO SOUND"
		return NoneSound()
	fullname = os.path.join(data_dir, name)
	try:
		sound = pygame.mixer.Sound(fullname)
	except pygame.error:
		print ('Cannot load sound: %s' % fullname)
		raise SystemExit(str(geterror()))
	return sound


class Map:

	def __init__(self):
		self.GameObjects = []
		self.Walls = []
		self.Switches = []
		self.player = None
		self.Stars = []
		self.reload = None
		self.next = None
		self.arrow = None
		self.clear = False
		self.ceremony_please = False

	def addwall(self, wall):
		global dirty
		dirty = True
		self.Walls.append(wall)
		self.GameObjects.append(wall)
		wall.map = self

	def addwalls(self, walls):
		for wall in walls:
			self.addwall(wall)

	def addplayer(self, player = None):
		global dirty
		dirty = True
		if not player:
			player = Player()
		self.player = player
		self.addarrow(player.arrow)
		self.GameObjects.append(player)
		player.map = self


	def addstar(self, star):
		global dirty
		dirty = True
		self.Stars.append(star)
		self.GameObjects.append(star)
		star.map = self

	def addstars(self, stars):
		for star in stars:
			self.addstar(star)

	def addswitch(self, switch):
		global dirty
		dirty = True
		self.Switches.append(switch)
		self.GameObjects.append(switch)
		switch.map = self

	def addarrow(self, arrow):
		global dirty
		dirty = True
		self.GameObjects.append(arrow)
		arrow.map = self


class GameObject(pygame.sprite.Sprite):
	def __init__(self):
		pygame.sprite.Sprite.__init__(self)
		self.enabled = True
		self.map = None
		self.hideifdisabled = True

	def deactivate(self):
		global dirty
		dirty = True
		self.enabled = False


	def activate(self):
		global dirty
		dirty = True
		self.enabled = True

	def togglestate(self):
		global dirty
		dirty = True

		if self.enabled:
			self.deactivate()
		else:
			self.activate()

		if hasattr(self, 'change'):	
			self.change()

class Player(GameObject):

	def __init__(self, x, y):
		GameObject.__init__(self)
		self.image = PlayerImage[0]
		self.rect = PlayerImage[1].copy()
		"""new_image = pygame.transform.scale(self.image, (40,40))
		self.image = new_image"""
		screen = pygame.display.get_surface()
		self.area = screen.get_rect()
		self.xspeed = 3
		self.yspeed = 0
		self.g = 0.05
		self.onground = False
		self.rect.topleft = self.startpoint = self.savepoint = x, y
		self.alive = True
		self.saved_yspeed = 0
		self.arrow = Arrow()
		self.arrow.rect.topleft = self.rect.topleft[0] + 4, self.rect.topleft[1] - 32

	def update(self):
		self.lastonground = self.onground
		self.onground = False
		self.yspeed += self.g
		if self.yspeed > 10:
			self.yspeed = 10
		self.move(0, 1)

		# Onground = True if on ground
		self.rect.bottom += 2
		for wall in currentmap.Walls:
			if self.rect.colliderect(wall.rect) and wall.enabled:
				self.onground = True
		
		if self.yspeed < 0:
			self.onground = False

		if self.onground or not self.lastonground:
			self.rect.bottom -= 2

		self.checkalive()

	
		for obj in currentmap.GameObjects:
			obj.hideifdisabled = True

		for switch in currentmap.Switches:
			if currentmap.player.rect.colliderect(switch.rect):
				for obj in switch.connectedobjs:
					obj.hideifdisabled = False

		if self.rect.colliderect(self.map.Stars[0].rect) and self.map.Stars[0].enabled:
			self.map.clear = True

	def move(self, dx, dy):
		# Move each axis separately. Note that this checks for collisions both times.
		if dx != 0:
			self.move_single_axis(dx * self.xspeed, 0)
		if dy != 0:
			self.move_single_axis(0, dy * self.yspeed)

	def jump(self):
		if (self.onground and self.yspeed >= 0):
			self.yspeed = -2.1

	def move_single_axis(self, dx, dy):
		
		# Move the rect
		self.rect.x += dx
		self.rect.y += dy

		# If you collide with a wall, move out based on velocity
		for wall in currentmap.Walls:
			if self.rect.colliderect(wall.rect) and wall.enabled:
				if dx > 0: # Moving right; Hit the left side of the wall
					self.rect.right = wall.rect.left
				elif dx < 0: # Moving left; Hit the right side of the wall
					self.rect.left = wall.rect.right
				elif dy > 0: # Moving down; Hit the top side of the wall
					self.rect.bottom = wall.rect.top
					self.yspeed = 0
				elif dy < 0: # Moving up; Hit the bottom side of the wall
					self.rect.top = wall.rect.bottom
					self.yspeed = 0



	def save(self):
		self.savepoint = self.rect.x, self.rect.y
		self.saved_yspeed = self.yspeed
		self.arrow.rect.topleft = self.rect.topleft[0] + 4, self.rect.topleft[1] - 32

	def load(self):
		self.youcantgo = self.rect.x, self.rect.y
		self.youcantspeed = self.yspeed
		self.rect.x, self.rect.y = self.savepoint
		self.yspeed = self.saved_yspeed

		#you can't load if a gray wall is placed
		for wall in currentmap.Walls:
			if self.rect.colliderect(wall.rect) and wall.enabled and wall.gray:
				self.rect.x, self.rect.y = self.youcantgo
				self.yspeed = self.youcantspeed
				return

	def respawn(self):
		self.rect.x, self.rect.y = self.startpoint
		self.alive = True
		self.yspeed = 0

	def checkalive(self):
		if self.rect.topleft[0] < -60:
			self.alive = False
		if self.rect.topleft[0] > (SCREENWIDTH + 40):
			self.alive = False
		if self.rect.topleft[1] < -60:
			self.alive = False
		if self.rect.topleft[1] > (SCREENHEIGHT + 40):
			self.alive = False

		if self.alive == False:
			self.respawn()


class Wall(GameObject):
	def __init__(self, pos_x = 0, pos_y = 0, size_x = 64, size_y = 64, gray = False, width = True):
		GameObject.__init__(self)
		self.gray = gray
		self.x = size_x
		self.y = size_y
		self.pos_x = pos_x
		self.pos_y = pos_y
		self.width = width
		
		if gray == True and not self.enabled:
			self.image, self.rect = GrayDisabled
		elif gray == True and width == True:
			self.image, self.rect = GrayWEnabled
		elif gray == True and width == False:
			self.image, self.rect = GrayHEnabled
		elif gray == False and width == True:
			self.image, self.rect = BlackWidth
		elif gray == False and width == False:
			self.image, self.rect = BlackHeight
		else:
			print "Ami.turn_left"

		screen = pygame.display.get_surface()
		self.area = screen.get_rect()

		new_image = pygame.transform.scale(self.image, (size_x,size_y))
		self.image = new_image
		self.rect = Rect(pos_x, pos_y, size_x, size_y)

	def change(self):


		if self.gray == True and not self.enabled:
			self.image, self.rect = GrayDisabled
		elif self.gray == True and self.width == True:
			self.image, self.rect = GrayWEnabled
		elif self.gray == True and self.width == False:
			self.image, self.rect = GrayHEnabled
		else:
			print "Hubo.move"

		screen = pygame.display.get_surface()
		self.area = screen.get_rect()

		new_image = pygame.transform.scale(self.image, (self.x, self.y))
		self.image = new_image
		self.rect = Rect(self.pos_x, self.pos_y, self.x, self.y)





class Switch(GameObject):
	def __init__(self, x = 0, y = 0, connectedobjs = [], action = None, blue = False):
		GameObject.__init__(self)
		if blue:
			self.image, self.rect = BSwitchImage[0], BSwitchImage[1].copy()
		else:
			self.image, self.rect = RSwitchImage[0], RSwitchImage[1].copy()
		screen = pygame.display.get_surface()
		self.area = screen.get_rect()
		self.rect.topleft = x, y
		if not action:
			action = self.toggle_default
		self.action = action
		self.connectedobjs = connectedobjs

	def toggle_default(self):
		for gameobject in self.connectedobjs:
			gameobject.togglestate()

	def teleport(self):
		assert len(self.connectedobjs) == 1
		self.map.player.rect.topleft = self.connectedobjs[0].rect.topleft



class Star(GameObject):
	def __init__(self,pos_x,pos_y,color):
		GameObject.__init__(self)
		if color == "Green":
			self.image, self.rect = load_image('Stars/green.png',-1)
		elif color == "Red":
			self.image, self.rect = load_image('Stars/red.png',-1)
		elif color == "Yellow":
			self.image, self.rect = load_image('Stars/yellow.png',-1)
		elif color == "Orange":
			self.image, self.rect = load_image('Stars/orange.png',-1)
		elif color == "Blue":
			self.image, self.rect = load_image('Stars/blue.png',-1)
		elif color == "Purple":
			self.image, self.rect = load_image('Stars/purple.png',-1)
		elif color == "Black":
			self.image, self.rect = load_image('Stars/black.png',-1)

		screen = pygame.display.get_surface()
		self.area = screen.get_rect()
		self.rect.topleft = pos_x,pos_y

		self.pos_x = pos_x
		self.pos_y = pos_y
		self.color = color
		self.ready = 0

	"""
	def Spin(self,angle_speed):
		center = self.rect.center
		self.image = pygame.transform.rotate(self.image,angle_speed)
		self.rect = self.image.get_rect(center = center)
	"""

	def move(self):
		if self.ready == 2:
			new_star = Star(self.pos_x,self.pos_y,self.color)
			self.map.addstar(new_star)
			self.rect.topleft = self.pos_x-40, self.pos_y
			self.pos_x -= 40
			self.ready = 0
		else:
			self.ready += 1


def create_many_stars():
	star1 = Star(800, 0, "Yellow")
	star2 = Star(905, 50, "Red")
	star3 = Star(1010, 100, "Blue")
	star4 = Star(1115, 150, "Orange")
	star5 = Star(1220, 200, "Green")
	star6 = Star(1325, 250, "Purple")
	star7 = Star(1430, 300, "Black")
	star8 = Star(1535, 350, "Yellow")
	star9 = Star(1640, 400, "Red")
	star10 = Star(1745, 450, "Blue")
	star11 = Star(1850, 500, "Orange")
	star12 = Star(1955, 550, "Green")
	star13 = Star(2060, 600, "Purple")
	some_stars = [star1, star2, star3, star4, star5, star6, star7, star8, star9, star10, star11, star12, star13]

	return some_stars

	def move(self,speed):
		new_star = Star(self.pos_x,self.pos_y,self.color)
		self.map.addstar(new_star)
		self.rect.topleft = self.pos_x-speed, self.pos_y
		self.pos_x -= speed



class Arrow(GameObject):
	def __init__(self):
		GameObject.__init__(self)
		self.image, self.rect = ArrowImage[0], ArrowImage[1].copy()
		screen = pygame.display.get_surface()
		self.area = screen.get_rect()



def Stage0():

	stage0 = Map()
	wall = Wall(75, 425, 300, 100)
	wall2 = Wall(450, 425, 300, 100)
	player = Player(100, 350)
	#switch = Switch(125, 330, [wall])
	star = Star(700,350,"Red")
	some_stars = create_many_stars()
	some_stars.insert(0, star)

	'''
	telposwitch1 = Switch(400, 350, [])
	telposwitch2 = Switch(300, 350, [telposwitch1])
	telposwitch1.connectedobjs = [telposwitch2]
	telposwitch1.action = telposwitch1.teleport
	telposwitch2.action = telposwitch2.teleport
	stage0.addswitch(telposwitch1)
	stage0.addswitch(telposwitch2)
	'''
	stage0.addstars(some_stars)
	stage0.addwall(wall)
	stage0.addwall(wall2)
	stage0.addplayer(player)
	#stage0.addswitch(switch)
	
	#Create The Background
	bgd = BackgroundImageSurface
	background = pygame.Surface((SCREENWIDTH, SCREENHEIGHT))
	background = background.convert()
	background.blit(bgd, (0, 0))

	#Put Text On The Background
	if pygame.font:
		font = pygame.font.Font(None, 40)
		text = font.render("Move : Left, Right / Jump : Up", 1, (10, 10, 10))
		textpos = text.get_rect(centerx=background.get_width()/2)

		background.blit(text, textpos)

	stage0.background = background
	stage0.reload = Stage0
	stage0.next = Stage1

	return stage0


def Stage1():

	stage1 = Map()
	wall1 = Wall(75, 250, 150, 800, False, False)
	wall2 = Wall(575, 250, 150, 800, False, False)
	wall3 = Wall(225, 500, 350, 800, False, False)
	bridge = Wall(225, 250, 350, 50, True) 
	bridge.togglestate()
	player = Player(625, 200)
	switch = Switch(300, 450, [bridge])
	star = Star(150, 200, "Red")
	some_stars = create_many_stars()
	some_stars.insert(0, star)

	stage1.addstars(some_stars)
	stage1.addwall(wall1)
	stage1.addwall(wall2)
	stage1.addwall(wall3)
	stage1.addwall(bridge)
	stage1.addswitch(switch)
	stage1.addplayer(player)

	
	bgd = BackgroundImageSurface
	background = pygame.Surface((SCREENWIDTH, SCREENHEIGHT))
	background = background.convert()
	background.blit(bgd, (0, 0))

	if pygame.font:
		font = pygame.font.Font(None, 40)
		text = font.render("Z to save, X to load, E to interact, R to restart", 1, (10, 10, 10))
		textpos = text.get_rect(centerx=background.get_width()/2)

		background.blit(text, textpos)

	stage1.background = background
	stage1.reload = Stage1
	stage1.next = Stage1to2

	return stage1


def Stage1to2():
	stage1to2 = Map()
	wall1 = Wall(0, 0, 50, 250, False, False)
	wall2 = Wall(0, 250, 150, 350, False, False)
	wall3 = Wall(150, 400, 200, 200, False, False)
	wall4 = Wall(350, 250, 100, 350, False, False)
	wall5 = Wall(450, 400, 200, 200, False, False)
	wall6 = Wall(650, 250, 100, 350, False, False)
	bridge1 = Wall(150, 250, 200, 50, True)
	bridge2 = Wall(450, 250, 200, 50, True)
	bridge2.togglestate()
	player = Player(370, 200)
	switch = Switch(80, 200, [bridge1, bridge2])
	star = Star(670, 200, "Red")
	some_stars = create_many_stars()
	some_stars.insert(0, star)

	stage1to2.addstars(some_stars)
	stage1to2.addwalls([wall1, wall2, wall3, wall4, wall5, wall6, bridge1, bridge2])
	stage1to2.addplayer(player)

	stage1to2.addswitch(switch)

	#Create The Background
	bgd = BackgroundImageSurface
	background = pygame.Surface((SCREENWIDTH, SCREENHEIGHT))
	background = background.convert()
	background.blit(bgd, (0, 0))

	#Put Text On The Background
	if pygame.font:
		font = pygame.font.Font(None, 40)
		text = font.render("Z to save, X to load, E to interact, R to restart", 1, (10, 10, 10))
		textpos = text.get_rect(centerx=background.get_width()/2)

		background.blit(text, textpos)

	stage1to2.background = background
	stage1to2.reload = Stage1to2
	stage1to2.next = Stage2

	return stage1to2


def Stage2():

	stage2 = Map()
	wall1 = Wall(50, 150, 100, 50)
	wall2 = Wall(400, 250, 200, 50)
	wall3 = Wall(700, 150, 100, 50)
	wall4 = Wall(100, 550, 100, 50)
	bridge1 = Wall(225, 200, 100, 50, True)
	bridge1.togglestate()
	bridge2 = Wall(550, 50, 50, 200, True, False)
	bridge2.togglestate()
	player = Player(735, 100)
	switch = Switch(110, 500, [bridge1, bridge2])
	star = Star(100, 100, "Red")
	some_stars = create_many_stars()
	some_stars.insert(0, star)

	stage2.addstars(some_stars)
	stage2.addwalls([wall1, wall2, wall3, wall4, bridge1, bridge2])
	stage2.addswitch(switch)
	stage2.addplayer(player)

	bgd = BackgroundImageSurface
	background = pygame.Surface((SCREENWIDTH, SCREENHEIGHT))
	background = background.convert()
	background.blit(bgd, (0, 0))

	if pygame.font:
		font = pygame.font.Font(None, 40)
		text = font.render("You can't load to the place where a gray wall is placed.", 1, (10, 10, 10))
		textpos = text.get_rect(centerx=background.get_width()/2)

		background.blit(text, textpos)

	stage2.background = background
	stage2.reload = Stage2
	stage2.next = Stage3

	return stage2


def Stage3():

	stage3 = Map()
	width_wall1 = Wall(0, 200, 200, 400, False, False)
	width_wall2 = Wall(300, 550, 475, 50)
	bridge1 = Wall(425, 50, 50, 500, True, False)
	bridge2 = Wall(600, 50, 50, 500, True, False)
	bridge2.togglestate()
	bridge3 = Wall(75, 50, 50, 150, True, False)
	player = Player(140, 150)
	switch1 = Switch(335, 500, [bridge1, bridge2])
	switch2 = Switch(730, 500, [bridge3])
	star = Star(20, 150, "Red")
	some_stars = create_many_stars()
	some_stars.insert(0, star)

	stage3.addstars(some_stars)
	stage3.addwalls([width_wall1, width_wall2, bridge1, bridge2, bridge3])
	stage3.addswitch(switch1)
	stage3.addswitch(switch2)
	stage3.addplayer(player)

	
	bgd = BackgroundImageSurface
	background = pygame.Surface((SCREENWIDTH, SCREENHEIGHT))
	background = background.convert()
	background.blit(bgd, (0, 0))

	if pygame.font:
		font = pygame.font.Font(None, 30)
		text = font.render("If you die, you go to the starting point WITHOUT resetting the map.", 1, (10, 10, 10))
		textpos = text.get_rect(centerx=background.get_width()/2)
		textpos[1] = 20

		background.blit(text, textpos)

	stage3.background = background
	stage3.reload = Stage3
	stage3.next = Stage4

	return stage3


def Stage4():

	stage4 = Map()
	width_wall1 = Wall(0, 550, 550, 50)
	width_wall2 = Wall(600, 550, 200, 50)
	width_wall3 = Wall(225, 250, 225, 50)
	width_wall4 = Wall(550, 350, 250, 50)
	width_wall5 = Wall(125, 450, 75, 50)
	width_wall6 = Wall(175, 350, 75, 50)
	height_wall1 = Wall(400, 300, 50, 250, False, False)
	height_wall2 = Wall(750, 450, 50, 150, False, False)
	height_wall3 = Wall(750, 0, 50, 350, False, False)

	bridge1 = Wall(0, 500, 50, 50, True)
	bridge2 = Wall(0, 400, 75, 50, True)
	bridge2.togglestate()
	bridge3 = Wall(0, 300, 125, 50, True)
	bridge4 = Wall(400, 50, 50, 200, True, False)
	bridge5 = Wall(450, 350, 100, 50, True)
	bridge5.togglestate()
	bridge6 = Wall(650, 500, 100, 50, True)
	bridge6.togglestate()
	player = Player(335, 510)
	switch1 = Switch(300, 500, [bridge1, bridge2, bridge3, bridge6])
	switch2 = Switch(300, 200, [bridge4, bridge5])
	star = Star(760, 410, "Blue")
	some_stars = create_many_stars()
	some_stars.insert(0, star)

	stage4.addstars(some_stars)
	stage4.addwalls([width_wall1, width_wall2, width_wall3, width_wall4, width_wall5, width_wall6, height_wall1, height_wall2, height_wall3])
	stage4.addwalls([bridge1, bridge2, bridge3, bridge4, bridge5, bridge6])
	stage4.addswitch(switch1)
	stage4.addswitch(switch2)
	stage4.addplayer(player)

	
	bgd = BackgroundImageSurface
	background = pygame.Surface((SCREENWIDTH, SCREENHEIGHT))
	background = background.convert()
	background.blit(bgd, (0, 0))

	if pygame.font:
		font = pygame.font.Font(None, 40)
		text = font.render("Hello, World?", 1, (10, 10, 10))
		textpos = text.get_rect(centerx=background.get_width()/2)

		background.blit(text, textpos)

	stage4.background = background
	stage4.reload = Stage4
	stage4.next = StageTeleportTutorial

	return stage4
	
def StageTeleportTutorial():

	tutomap = Map()
	hwall1 = Wall(0, 550, 150, 70)
	hwall2 = Wall(0, 200, 150, 70)
	hwall3 = Wall(400, 530, 150, 70)
	vbridge1 = Wall(70, 270, 40, 280, True, False)
	vbridge2 = Wall(360, 50, 40, 550, True, False)
	vbridge3 = Wall(550, 50, 40, 550, True, False)

	telposwitch1 = Switch(100, 150, [], None, True)
	telposwitch2 = Switch(100, 500, [telposwitch1], None, True)
	telposwitch1.connectedobjs = [telposwitch2]
	
	telposwitch1.action = telposwitch1.teleport
	telposwitch2.action = telposwitch2.teleport
	
	tutomap.addswitch(telposwitch1)
	tutomap.addswitch(telposwitch2)
	player = Player(20, 510)
	star = Star(20, 150, "Green")
	star.togglestate()
	vbridge1.togglestate()
	vbridge2.togglestate()
	vbridge3.togglestate()
	finalswitch = Switch(450, 500, [star, vbridge1, vbridge2, vbridge3])
	some_stars = create_many_stars()
	some_stars.insert(0, star)
	tutomap.addstars(some_stars)
	tutomap.addswitch(finalswitch)
	tutomap.addstar(star)
	tutomap.addwalls([hwall1, hwall2, hwall3, vbridge1, vbridge2, vbridge3])
	
	tutomap.addplayer(player)

	bgd = BackgroundImageSurface
	background = pygame.Surface((SCREENWIDTH, SCREENHEIGHT))
	background = background.convert()
	background.blit(bgd, (0, 0))

	if pygame.font:
		font = pygame.font.Font(None, 40)
		text = font.render("These green switches act as portals...", 1, (10, 10, 10))
		textpos = text.get_rect(centerx=background.get_width()/2)

		background.blit(text, textpos)

	tutomap.background = background
	tutomap.reload = StageTeleportTutorial
	tutomap.next = Stagehard

	return tutomap


def Stagehard():

	stagehard = Map()
	width_wall1 = Wall(100, 100, 600, 100)
	width_wall2 = Wall(100, 300, 600, 100)
	width_wall3 = Wall(100, 500, 600, 100)
	height_wall1 = Wall(50, 0, 50, 400, False, False)
	height_wall2 = Wall(700, 100, 50, 500, False, False)
	height_wall3 = Wall(50, 500, 50, 100, False, False)
	bridge1 = Wall(250, 400, 50, 100, True, False)
	bridge1.togglestate()
	bridge2 = Wall(500, 400, 50, 100, True, False)
	#bridge2.togglestate()
	bridge3 = Wall(250, 200, 50, 100, True, False)
	#bridge3.togglestate()
	bridge4 = Wall(500, 200, 50, 100, True, False)
	#bridge4.togglestate()
	bridge5 = Wall(250, 0, 50, 100, True, False)
	#bridge5.togglestate()
	bridge6 = Wall(500, 0, 50, 100, True, False)
	#bridge6.togglestate()
	bridge7 = Wall(50, 400, 50, 100, True, False)
	#bridge7.togglestate()
	bridge8 = Wall(700, 0, 50, 100, True, False)
	#bridge8.togglestate()
	bridge9 = Wall(150, 400, 50, 100, True, False)
	player = Player(305, 440)
	switch1 = Switch(345, 450, [bridge1, bridge4])
	switch2 = Switch(600, 450, [bridge1, bridge7])
	switch3 = Switch(445, 250, [bridge5])
	switch4 = Switch(345, 50, [bridge3, bridge5])
	switch5 = Switch(600, 50, [bridge1, bridge2, bridge3, bridge6, bridge8, bridge9])
	switch6 = Switch(125, 250, [bridge4, bridge6, bridge9])

	telposwitch1 = Switch(445, 450, [], None, True)
	telposwitch2 = Switch(625, 250, [telposwitch1], None, True)
	telposwitch3 = Switch(345, 250, [], None, True)
	telposwitch4 = Switch(125, 50, [telposwitch3], None, True)
	telposwitch1.connectedobjs = [telposwitch2]
	telposwitch1.action = telposwitch1.teleport
	telposwitch2.action = telposwitch2.teleport
	telposwitch3.connectedobjs = [telposwitch4]
	telposwitch3.action = telposwitch3.teleport
	telposwitch4.action = telposwitch4.teleport

	star = Star(0, 450, "Red")	
	some_stars = create_many_stars()
	some_stars.insert(0, star)

	stagehard.addstars(some_stars)
	stagehard.addwalls([width_wall1, width_wall2, width_wall3, height_wall1, height_wall2, height_wall3])
	stagehard.addwalls([bridge1, bridge2, bridge3, bridge4, bridge5, bridge6, bridge7, bridge8, bridge9])
	stagehard.addswitch(switch1)
	stagehard.addswitch(switch2)
	stagehard.addswitch(switch3)
	stagehard.addswitch(switch4)
	stagehard.addswitch(switch5)
	stagehard.addswitch(switch6)
	stagehard.addswitch(telposwitch1)
	stagehard.addswitch(telposwitch2)
	stagehard.addswitch(telposwitch3)
	stagehard.addswitch(telposwitch4)
	stagehard.addplayer(player)
	
	bgd = BackgroundImageSurface
	background = pygame.Surface((SCREENWIDTH, SCREENHEIGHT))
	background = background.convert()
	background.blit(bgd, (0, 0))

	if pygame.font:
		font = pygame.font.Font(None, 40)
		text = font.render("Thief is weak", 1, (10, 10, 10))
		textpos = text.get_rect(centerx=background.get_width()/2)

		background.blit(text, textpos)

	stagehard.background = background
	stagehard.reload = Stagehard
	stagehard.next = Stagefinish

	return stagehard

'''def Stagemedium():

	stagemedium = Map()
	hor_wall_1 = Wall(0,550,100,50)
	hor_wall_2 = Wall(200,550,500,50)
	hor_wall_3 = Wall(0,400,500,5)
	hor_wall_4 = Wall(700,350,100,5)
	hor_wall_5 = Wall(650,270,150,5) 
	hor_wall_6 = Wall(0,170,100,5)
	hor_wall_7 = Wall(700,170,100,5)
	hor_wall_8 = Wall(0,100,800,5)
	hor_wall_9 = Wall(0,50,800,5)
	ver_wall_1 = Wall(700,450,100,200,False,False)
	ver_wall_2 = Wall(645,270,5,135,False,False)

	bridge1 = Wall(100, 550, 100, 50, True, False)
	bridge2 = Wall(650, 500, 50, 50, True, False)
	bridge3 = Wall(500, 400, 145, 5, True, True)
	bridge4 = Wall(700, 355, 100, 95, True, False)
	bridge5 = Wall(700, 275, 25, 75, True, False)
	bridge6 = Wall(650, 400, 25, 5, True, True)
	bridge7 = Wall(400, 0, 50, 50, True, False)
	bridge8 = Wall(500, 220, 150, 20, True, True)
	bridge9 = Wall(600, 0, 50, 50, True, False)

	bridge5.togglestate()
	bridge6.togglestate()
	bridge8.togglestate()
	#bridge4.togglestate()
	bridge7.togglestate()
	bridge9.togglestate()

	switch1 = Switch(300, 65, [bridge1, bridge2, bridge3])
	switch2 = Switch(500, 65, [bridge4, bridge5])
	switch3 = Switch(700, 65, [bridge6, bridge7])
	switch4 = Switch(0, 130, [bridge8, bridge9])
	telposwitch1 = Switch(300, 15, [], None, True)
	telposwitch2 = Switch(0, 65, [telposwitch1], None, True)
	telposwitch3 = Switch(500, 15, [], None, True)
	telposwitch4 = Switch(0, 350, [telposwitch3], None, True)
	telposwitch5 = Switch(700, 15, [], None, True)
	telposwitch6 = Switch(750, 220, [telposwitch5], None, True)
	telposwitch7 = Switch(50, 130, [], None, True)
	telposwitch8 = Switch(750, 300, [telposwitch7], None, True)

	telposwitch1.connectedobjs = [telposwitch2]
	telposwitch1.action = telposwitch1.teleport
	telposwitch2.action = telposwitch2.teleport
	telposwitch3.connectedobjs = [telposwitch4]
	telposwitch3.action = telposwitch3.teleport
	telposwitch4.action = telposwitch4.teleport

	telposwitch5.connectedobjs = [telposwitch6]
	telposwitch5.action = telposwitch5.teleport
	telposwitch6.action = telposwitch6.teleport
	telposwitch7.connectedobjs = [telposwitch8]
	telposwitch7.action = telposwitch7.teleport
	telposwitch8.action = telposwitch8.teleport
	stagemedium.addswitch(telposwitch1)
	stagemedium.addswitch(telposwitch2)
	stagemedium.addswitch(telposwitch3)
	stagemedium.addswitch(telposwitch4)
	stagemedium.addswitch(telposwitch5)
	stagemedium.addswitch(telposwitch6)
	stagemedium.addswitch(telposwitch7)
	stagemedium.addswitch(telposwitch8)

	player = Player(0, 0)
	star = Star(750, 110, "Red")

	stagemedium.addwalls([hor_wall_9,hor_wall_8,hor_wall_7,hor_wall_6,hor_wall_5,hor_wall_4,hor_wall_3,hor_wall_2,hor_wall_1,ver_wall_1,ver_wall_2])
	stagemedium.addwalls([bridge1, bridge2, bridge3, bridge4, bridge5, bridge6, bridge7, bridge8, bridge9])
	stagemedium.addplayer(player)
	stagemedium.addswitch(switch1)
	stagemedium.addswitch(switch2)
	stagemedium.addswitch(switch3)
	stagemedium.addswitch(switch4)
	stagemedium.addstar(star)

	bgd = BackgroundImageSurface
	background = pygame.Surface((SCREENWIDTH, SCREENHEIGHT))
	background = background.convert()
	background.blit(bgd, (0, 0))

	stagemedium.background = background
	stagemedium.reload = Stagemedium
	stagemedium.next = Stage0

	return stagemedium'''


def Stagefinish():

	stagefinish = Map()
	width_wall1 = Wall(0, 500, 800, 100)
	player = Player(0, 380)
	star = Star(1000, 0, "Red")
	some_stars = create_many_stars()
	some_stars.insert(0, star)
	stagefinish.ceremony_please = True

	stagefinish.addstars(some_stars)
	stagefinish.addwall(width_wall1)
	stagefinish.addplayer(player)

	bgd = BackgroundImageSurface
	background = pygame.Surface((SCREENWIDTH, SCREENHEIGHT))
	background = background.convert()
	background.blit(bgd, (0, 0))

	if pygame.font:
		font = pygame.font.Font(None, 70)
		text = font.render("Congratulations!!!", 1, (10, 10, 10))
		textpos = text.get_rect(centerx=background.get_width()/2)
		textpos[1] = 20

		background.blit(text, textpos)

	stagefinish.background = background
	stagefinish.reload = Stagefinish
	stagefinish.next = Stage0

	return stagefinish



def main():
	global currentmap
	global dirty
	global PlayerImage, BlackWidth, BlackHeight, GrayWEnabled, GrayHEnabled, GrayDisabled
	global RSwitchImage, BSwitchImage, ArrowImage, BackgroundImageSurface
	global GreenStar, RedStar, YellowStar, BlueStar, PinkStar, BlackStar

	pygame.init()
	screen = pygame.display.set_mode((SCREENWIDTH, SCREENHEIGHT))

	PlayerImage = load_image('resources/Player_original.png')

	BlackWidth = load_image('resources/Wall_original_horizontal_new.png')
	BlackHeight = load_image('resources/Wall_original_vertical.png')
	GrayWEnabled = load_image('resources/Bridge_horizontal.png')
	GrayHEnabled = load_image('resources/Bridge_vertical.png')
	GrayDisabled = load_image('resources/Wall_original_trace.png', -2)
	RSwitchImage = load_image('resources/Switch_original_half.png', -1)
	BSwitchImage = load_image('resources/Switch_green.png', -1)
	BackgroundImageSurface = load_image('resources/background.png')[0]
	ArrowImage = load_image('resources/Arrow.png', -2)

	GreenStar = load_image('Stars/Green Star.png', -1)
	RedStar = load_image('Stars/Red Star.png', -1)
	YellowStar = load_image('Stars/Yellow Star.png', -1)
	OrangeStar = load_image('Stars/Orange Star.png', -1)
	BlueStar = load_image('Stars/Blue Star.png', -1)
	PinkStar = load_image('Stars/Pink Star.png', -1)
	BlackStar = load_image('Stars/Black Star.png', -1)


	#start stage
	currentmap = Stage0()
	newstage = None
	ceremonycounter = 0

	"""star = Star(600,100,"Red")
	currentmap.addstar(star)"""

	visibleobjs = filter(lambda obj: obj.enabled or not obj.hideifdisabled, currentmap.GameObjects)
	allsprites = pygame.sprite.OrderedUpdates(visibleobjs)

	#bgm = load_sound('resources/01.mp3')

	if pygame.mixer:
		music = os.path.join(main_dir, 'Resources', 'resources/BGM.ogg')
		pygame.mixer.music.load(music)
		pygame.mixer.music.play(-1)

	while True:
		clock.tick(60)

		if newstage:
			currentmap = newstage
			newstage = None
			dirty = True

		#if Dirty:
		if True:
			visibleobjs = filter(lambda obj: obj.enabled or not obj.hideifdisabled, currentmap.GameObjects)
			allsprites = pygame.sprite.OrderedUpdates(visibleobjs)
			dirty = False

		for event in pygame.event.get():
			if event.type == pygame.QUIT:
				sys.exit()

			if currentmap.clear: break

			if event.type == pygame.KEYDOWN and event.key == K_e:

				for switch in filter(lambda s: currentmap.player.rect.colliderect(s.rect) and s.enabled, currentmap.Switches):
					switch.action()

			if event.type == pygame.KEYDOWN and event.key == K_r:
				if not newstage:
					newstage = currentmap.reload()

			if event.type == pygame.KEYDOWN and event.key == K_z:
				currentmap.player.save()
		
			if event.type == pygame.KEYDOWN and event.key == K_x:
				currentmap.player.load()

			if event.type == pygame.KEYDOWN and event.key == K_END:
				currentmap.clear = True

		#get input
		keystate = pygame.key.get_pressed()

		#handle player input
		direction_lr = keystate[K_RIGHT] - keystate[K_LEFT]
		direction_ud = keystate[K_UP]
		if not currentmap.clear:
			currentmap.player.move(direction_lr, 0)
			if direction_ud == 1:
				if currentmap.player.onground == True:
					currentmap.player.jump()

		currentmap.player.update()

		"""if keystate[K_v]:
			star.move(10)"""				

		if currentmap.clear == True and ceremonycounter < 150:
			for star in currentmap.Stars[1:13]:
				star.move()
			ceremonycounter += 1

		if currentmap.clear == True and ceremonycounter >= 150:
			newstage = currentmap.next()
			ceremonycounter = 0

		if currentmap.ceremony_please:
			if ceremonycounter < 150:
				for star in currentmap.Stars[1:13]:
					star.move()
			ceremonycounter += 1
			if ceremonycounter >= 300:
				ceremonycounter = 0
				newstage = currentmap

		allsprites.update()
		screen.blit(currentmap.background, (0, 0))
		allsprites.draw(screen)
		pygame.display.flip()

if __name__ == '__main__':
	main()